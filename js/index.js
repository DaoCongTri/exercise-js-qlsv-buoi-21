var ArrSV = [];
var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var sinhvien = new sinhVien(
      item.masv,
      item.tensv,
      item.password,
      item.emailsv,
      item.diemtoan,
      item.diemly,
      item.diemhoa
    );
    ArrSV.push(sinhvien);
  }
  renderArrSV(ArrSV);
}
function themSinhVien() {
  var sinhvien = layThongTinTuForm();
  var isValid =
    kiemTraRong("spanMaSV", sinhvien.masv) &&
    kiemTraMaSvTrung(sinhvien.masv, ArrSV);
  isValid = isValid & kiemTraRong("spanTenSV", sinhvien.tensv);
  isValid =
    isValid & 
    kiemTraRong("spanEmailSV", sinhvien.emailsv) &&
    kiemTraEmail(sinhvien.emailsv) &&
    kiemTraEmailTrung(sinhvien.emailsv, ArrSV);
  isValid =
    isValid &
    kiemTraRong("spanMatKhau", sinhvien.password) &
    kiemTraRong("spanToan", sinhvien.diemtoan) &
    kiemTraRong("spanLy", sinhvien.diemly) &
    kiemTraRong("spanHoa", sinhvien.diemhoa);
    if (isValid) {
        ArrSV.push(sinhvien);
        var dataJson = JSON.stringify(ArrSV);
        localStorage.setItem("DSSV_LOCAL", dataJson);
    };
    renderArrSV(ArrSV);
};
function xoaSinhVien(id) {
  var vitri = -1;
  for (var i = 0; i < ArrSV.length; i++) {
    var sinhvien = ArrSV[i];
    if (sinhvien.masv == id) {
      vitri = i;
      break;
    }
  }
  ArrSV.splice(vitri, 1);
  renderArrSV(ArrSV);
};
function suaSV(id) {
  var vitri = ArrSV.findIndex((item) => {
    return item.masv == id;
  });
  if (vitri != -1) {
    document.getElementById("txtMaSV").disabled = true;
    showThongTinLenForm(ArrSV[vitri]);
  };
};
function capNhatSinhVien() {
  document.getElementById("txtMaSV").disabled = false;
  var sinhvien = layThongTinTuForm();
  var vitri = ArrSV.findIndex((item) => {
    return item.masv == sinhvien.masv;
  });
  if (vitri !== -1) {
    ArrSV[vitri] = sinhvien;
    renderArrSV(ArrSV);
    resetForm();
  };
};
function resetForm() {
  document.getElementById("formQLSV").reset();
};

