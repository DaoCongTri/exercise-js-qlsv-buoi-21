var showMessage = function(id, message){
    document.getElementById(id).innerHTML = message;
};
var kiemTraMaSvTrung = function(masv, ArrSV){
    var index = ArrSV.findIndex((item) => {
        return masv == item.masv;
    });
    if (index == -1) {
        showMessage("spanMaSV", "");
        return true;
    }else{
        showMessage("spanMaSV", `<p>Mã sinh viên bạn nhập bị trùng</p>`);
        return false;
    };
};
var kiemTraEmailTrung = function(emailsv, ArrSV){
    var index = ArrSV.findIndex((item) => {
        return emailsv == item.emailsv;
    });
    if (index == -1) {
        showMessage("spanEmailSV", "");
        return true;
    }else{
        showMessage("spanEmailSV", `<p>Email bạn nhập bị trùng</p>`);
        return false;
    };
};
var kiemTraEmail = function(emailsv){
    const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (re.test(emailsv)) {
        showMessage("spanEmailSV", "");
        return true;
    }else{
        showMessage("spanEmailSV", "Email không hợp lệ");
        return false;
    };
};
var kiemTraRong = function(idErr, value){
    if (value.length == 0 || value == 0) {
        showMessage(idErr, "Mục này không được bỏ trống! Vui lòng nhập");
        return false;
    }else {
        showMessage(idErr, "");
        return true;
    };
};
