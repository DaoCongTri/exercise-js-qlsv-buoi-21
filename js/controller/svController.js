function renderArrSV(ArrSV){
    var contentHTML = '';
    for (var i = 0; i < ArrSV.length; i++){
        var sinhvien = ArrSV[i];
        var contentTr = `
            <tr>
                <td>${sinhvien.masv}</td>
                <td>${sinhvien.tensv}</td>
                <td>${sinhvien.emailsv}</td>
                <td>${sinhvien.dtb().toFixed(2)}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaSinhVien(${sinhvien.masv})">Xóa</button>
                    <button class="btn btn-success" onclick="suaSV(${sinhvien.masv})">Sửa</button>
                </td>
            </tr>
        `;
        contentHTML += contentTr;
    };
    document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};
function layThongTinTuForm(){
    var masv = document.getElementById("txtMaSV").value;
    var tensv = document.getElementById("txtTenSV").value;
    var password = document.getElementById("txtPass").value;
    var emailsv = document.getElementById("txtEmail").value;
    var diemtoan = document.getElementById("txtDiemToan").value * 1;
    var diemly = document.getElementById("txtDiemLy").value * 1;
    var diemhoa = document.getElementById("txtDiemHoa").value * 1;
    var sinhvien = new sinhVien(masv, tensv, password, emailsv, diemtoan, diemly, diemhoa);
    return sinhvien;
};
function showThongTinLenForm(sinhvien){
    document.getElementById("txtMaSV").value = sinhvien.masv;
    document.getElementById("txtTenSV").value = sinhvien.tensv;
    document.getElementById("txtEmail").value = sinhvien.emailsv;
    document.getElementById("txtPass").value = sinhvien.password;
    document.getElementById("txtDiemToan").value = sinhvien.diemtoan;
    document.getElementById("txtDiemLy").value = sinhvien.diemly;
    document.getElementById("txtDiemHoa").value = sinhvien.diemhoa;
};